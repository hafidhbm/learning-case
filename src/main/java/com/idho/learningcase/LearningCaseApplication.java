package com.idho.learningcase;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class LearningCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningCaseApplication.class, args);
	}

}
